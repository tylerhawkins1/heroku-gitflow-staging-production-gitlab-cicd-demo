# Heroku and GitLab CI/CD Demo - Using Gitflow with Staging and Production Apps

Demo app deployed to Heroku using GitLab CI/CD. This repo uses a Gitflow branching model, with a `dev` and `main` branch. When code is merged into the `dev` branch, the app is deployed to the Heroku staging environment. When code is merged into the `main` branch, the app is deployed to the Heroku production environment.

## Running Locally

Make sure you have [Node.js](http://nodejs.org/) and the [Heroku CLI](https://cli.heroku.com/) installed.

```sh
$ git clone https://gitlab.com/tylerhawkins1/heroku-gitflow-staging-production-gitlab-ci-cd-demo.git
$ cd heroku-gitflow-staging-production-gitlab-ci-cd-demo
$ npm install
$ npm start
```

Your app should now be running on [localhost:5001](http://localhost:5001/).

## Deploying to Heroku

```
$ git checkout main
$ heroku create heroku-gitlab-ci-cd-production --remote heroku-production
$ git push heroku-production main
$ heroku open --remote heroku-production

$ git checkout dev
$ heroku create heroku-gitlab-ci-cd-staging --remote heroku-staging
$ git push heroku-staging main
$ heroku open --remote heroku-staging
```

## Resources

For more information about Heroku and GitLab CI/CD, see:

- https://devcenter.heroku.com/articles/getting-started-with-nodejs
- https://devcenter.heroku.com/articles/heroku-cli-commands
- https://docs.gitlab.com/ee/ci/cloud_deployment/heroku.html
- https://docs.gitlab.com/ee/ci/examples/deployment/
- https://docs.gitlab.com/ee/ci/variables/index.html
- https://forum.gitlab.com/t/ci-with-two-different-heroku-apps-and-branches/73723
- https://devcenter.heroku.com/articles/git
- https://devcenter.heroku.com/articles/multiple-environments
- https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow
